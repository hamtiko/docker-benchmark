FROM yiisoftware/yii2-php:7.2-apache

EXPOSE 80

COPY . /app

# Apache configurations
RUN rm -rf /etc/apache2/sites-available/000-default.conf
COPY docker/apache/000-default.conf /etc/apache2/sites-available/000-default.conf

WORKDIR /app

RUN php init --env=Development --overwrite=All
